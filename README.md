# Facilitando o uso de TCP no C

## Documentação das funções

A documentação foi feita intra-código e javadoc

## Build do módulo estático:

```
gcc -c tcp.c
```

para fazer uso simplesmente inclua o header no seu código fonte

```
#include "tcp.h"
```

e adicione o objeto à sua linha de compilação

```
gcc ... tcp.o ...
```

## Incluindo os fontes diretamente

para fazer uso simplesmente inclua o header no seu código fonte

```
#include "tcp.h"
```

e adicione o tcp.c à sua linha de compilação

```
gcc ... tcp.c ...
```

## Build dos exemplos

```
gcc -o server server.c tcp.c -pthread
gcc -o client client.c tcp.c -pthread
```
