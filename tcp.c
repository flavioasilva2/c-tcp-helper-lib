#include "tcp.h"

#ifndef _TCP_
#define _TCP_

int openTCPSocket(void) {
    int errLine;
    char *errFile;
    int socketFD = socket(AF_INET, SOCK_STREAM, 0); errLine = __LINE__; errFile = __FILE__;
    if (socketFD == -1) { /* Erro */
        if (errno == EACCES)
            fprintf(stderr, "EACCES: Permissão negada ao abrir socket. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EMFILE)
            fprintf(stderr, "EMFILE: O limete de descritores de arquivo foi atingido para esse processo. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ENFILE)
            fprintf(stderr, "ENFILE: O limite dearquivos abertos no sistema foi atingido. Arquivo %s linha %d\n", errFile, errLine);
        else if ((errno == ENOBUFS) || (errno == ENOMEM))
            fprintf(stderr, "ENOBUFS ou ENOMEM: Memória insuficiente. Arquivo %s linha %d\n", errFile, errLine);
        else
            fprintf(stderr, "Erro desconhecido ao abrir o socket TCP. Arquivo %s linha %d\n", errFile, errLine);
    }
    return socketFD;
}

int bindTCPSocket(char *ip, unsigned short port, int socketFD) {
    int status = 0, errLine;
    char *errFile;
    struct sockaddr_in serverAtIPPort;
    memset(&serverAtIPPort, '\0', sizeof(struct sockaddr_in));
    serverAtIPPort.sin_family = AF_INET;
    serverAtIPPort.sin_port = htons(port);
    if (strcmp(ip, "0.0.0.0") == 0) { /* Usar interface indefinida */
        serverAtIPPort.sin_addr.s_addr = htonl(INADDR_ANY);
    } else { /* Usar uma interface específica */
        if (!inet_aton(ip, &(serverAtIPPort.sin_addr))) { errLine = __LINE__; errFile = __FILE__; /* Erro na conversão de endereço */ 
            fprintf(stderr, "Erro na conversão de endereço da notação em octetos para notação em bytes na ordem de rede. Arquivo %s linha %d\n", errFile, errLine);
            status = 1;
            return status;
        }
    }
    if (bind(socketFD, (struct sockaddr *)&serverAtIPPort, sizeof(struct sockaddr_in)) != 0) { errLine = __LINE__; errFile = __FILE__; /* Erro ao executar bind */
        status = 1;
        if (errno == EACCES)
            fprintf(stderr, "EACCES: Permissão negada ao executar bind. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EADDRINUSE)
            fprintf(stderr, "EADDRINUSE: O endereço expecificado já está em uso. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EBADF)
            fprintf(stderr, "EBADF: O descritor de arquivo passado na invocação dessa função não é valido. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EINVAL)
            fprintf(stderr, "EINVAL: Bind já foi executado para esse socket ou a estrutura de endereçamento possui valores inconsistentes. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ENOTSOCK)
            fprintf(stderr, "ENOTSOCK: O descritor de arquivo passado na invocação dessa função não se refere à um socket. Arquivo %s linha %d\n", errFile, errLine);
        else
            fprintf(stderr, "Erro desconhecido ao executar o bind no socket. Arquivo %s linha %d\n", errFile, errLine);
        return status;
    }
    return status;
}

int listenOnTCPSocket(int socketFD) {
    int status = 0, errLine;
    char *errFile;
    if(listen(socketFD, SOMAXCONN) == -1) { errLine = __LINE__; errFile = __FILE__;
        status = 1;
        if (errno == EADDRINUSE)
            fprintf(stderr, "EADDRINUSE: Outro socket já está escutando nessa porta. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EBADF)
            fprintf(stderr, "EBADF: O descritor de arquivo passado na invocação dessa função não é valido. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ENOTSOCK)
            fprintf(stderr, "ENOTSOCK: O descritor de arquivo passado na invocação dessa função não se refere à um socket. Arquivo %s linha %d\n", errFile, errLine);
        else
            fprintf(stderr, "Erro desconhecido ao executar o listen no socket. Arquivo %s linha %d\n", errFile, errLine);
        return status;
    }
    return status;
}

TCPInfo acceptNewTCPConnection(int socketFD) {
    int errLine;
    char *errFile;
    TCPInfo clientTCPInfo;
    int newConnFD = accept(socketFD, (struct sockaddr*)&(clientTCPInfo.cliaddr), &(clientTCPInfo.addrlen)); errLine = __LINE__; errFile = __FILE__;
    clientTCPInfo.connectionFD = newConnFD;
    if (newConnFD == -1) {
        memset((void*)&clientTCPInfo, '\0', sizeof(TCPInfo));
        if (errno == EBADF)
            fprintf(stderr, "EBADF: Descritor de arquivo inválido. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ECONNABORTED)
            fprintf(stderr, "ECONNABORTED: Aconexão foi abortada. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EINTR)
            fprintf(stderr, "EINTR: Conexão interrompida por sinal. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EINVAL)
            fprintf(stderr, "EINVAL: O socket não está no estado ~listen~. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EMFILE)
            fprintf(stderr, "EMFILE: O limete de descritores de arquivo foi atingido para esse processo. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ENFILE)
            fprintf(stderr, "ENFILE: O limite dearquivos abertos no sistema foi atingido. Arquivo %s linha %d\n", errFile, errLine);
        else if ((errno == ENOBUFS) || (errno == ENOMEM))
            fprintf(stderr, "ENOBUFS ou ENOMEM: Memória insuficiente. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ENOTSOCK)
            fprintf(stderr, "ENOTSOCK: O descritor de arquivo passado na invocação dessa função não se refere à um socket. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EPROTO)
            fprintf(stderr, "EPROTO: Erro no protocolo. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EPERM)
            fprintf(stderr, "EPERM: Conexão não permitida devido a regras de firewall. Arquivo %s linha %d\n", errFile, errLine);
        else
            fprintf(stderr, "Erro desconhecido ao executar o accept no socket. Arquivo %s linha %d\n", errFile, errLine);
        return clientTCPInfo;
    }
    return clientTCPInfo;
}

int connectTCPSocket(char *ip, unsigned short port, int socketFD) {
    int status = 0, errLine;
    char *errFile;
    struct sockaddr_in serverAtIPPort;
    socklen_t addrlen = sizeof(struct sockaddr_in);
    memset(&serverAtIPPort, '\0', sizeof(struct sockaddr_in));
    serverAtIPPort.sin_family = AF_INET;
    serverAtIPPort.sin_port = htons(port);
    if (!inet_aton(ip, &(serverAtIPPort.sin_addr))) { errLine = __LINE__; errFile = __FILE__; /* Erro na conversão de endereço */ 
        fprintf(stderr, "Erro na conversão de endereço da notação em octetos para notação em bytes na ordem de rede. Arquivo %s linha %d\n", errFile, errLine);
        status = 1;
        return status;
    }
    if (connect(socketFD, (struct sockaddr*)&(serverAtIPPort), addrlen) == -1) { errLine = __LINE__; errFile = __FILE__;
        status = 1;
        if (errno == EACCES)
            fprintf(stderr, "EACCES: Acesso negado ao executar connect no socket. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EPERM)
            fprintf(stderr, "EPERM: Falha de conexão devido à uma regra de firewall. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EADDRINUSE)
            fprintf(stderr, "EADDRINUSE: Endereço local já em uso. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EADDRNOTAVAIL)
            fprintf(stderr, "EADDRNOTAVAIL: Todas as portas efemeras estão em uso. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EAGAIN)
            fprintf(stderr, "EAGAIN: Entradas insuficientes no cache de roteamento. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EBADF)
            fprintf(stderr, "EBADF: Descritor de arquivo inválido. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ECONNREFUSED)
            fprintf(stderr, "ECONNREFUSED: Conexão recusada. Tem alguém escutando na outra ponta?. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EINTR)
            fprintf(stderr, "EINTR: Conexão interrompida por um sinal. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EISCONN)
            fprintf(stderr, "EISCONN: Socket já conectado. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ENETUNREACH)
            fprintf(stderr, "ENETUNREACH: Rede indisponível. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ENOTSOCK)
            fprintf(stderr, "ENOTSOCK: O descritor de arquivo não se refere à um socket. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == ETIMEDOUT)
            fprintf(stderr, "ETIMEDOUT: Timeout na tentativa de conexão. Arquivo %s linha %d\n", errFile, errLine);
        else
            fprintf(stderr, "Erro desconhecido ao executar o connect no socket. Arquivo %s linha %d\n", errFile, errLine);
        return status;
    }
    return status;
}

int closeTCPSocket(int socketFD) {
    int status = 0, errLine;
    char *errFile;
    if (close(socketFD) != 0) { errLine = __LINE__; errFile = __FILE__;
        status = 1;
        if (errno == EBADF)
            fprintf(stderr, "EBADF: O descritor de arquivo passado na invocação dessa função não é valido. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EINTR) 
            fprintf(stderr, "EINTR: Close interrompido por um sinal. Arquivo %s linha %d\n", errFile, errLine);
        else if (errno == EIO) 
            fprintf(stderr, "EIO: Erro de E/S não expecificado. Arquivo %s linha %d\n", errFile, errLine);
        else
            fprintf(stderr, "Erro desconhecido ao executar close no socket. Arquivo %s linha %d\n", errFile, errLine);    
        return 1;
    }
    return status;
}

#endif /* _TCP_ */
