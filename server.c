#include <stdio.h>
#include <pthread.h>
#include "tcp.h"

#define MSG_LEN 128

const unsigned char buffersSize = MSG_LEN + 1;

unsigned char shouldExit = 0;

void *recvLoop(void *args);

int main(int argc, char **argv, char **env) {
    char send_buff[buffersSize];

    pthread_t recvLoopThreadID;
    void *recvLoopRetval;

    printf("Envie e receba mensagens de até %d caracteres.\n", MSG_LEN);
    printf("Para desconectar e sair digite exit.\n");

    int serverSocketFD = openTCPSocket();
    bindTCPSocket("127.0.0.1", 2197, serverSocketFD);
    listenOnTCPSocket(serverSocketFD);
    TCPInfo peerTCPInfo = acceptNewTCPConnection(serverSocketFD);

    pthread_create(&recvLoopThreadID, NULL, recvLoop, (void*)(&peerTCPInfo));

    while (!shouldExit) {
        memset(send_buff, '\0', buffersSize);
        printf("server> ");
        scanf("%s", send_buff);
        if (strcmp("exit", send_buff) == 0) {
            send(peerTCPInfo.connectionFD, send_buff, buffersSize, 0);
            printf("Saindo a pedido do server.\n");
            shouldExit = 1;
            fprintf(stdin, "exit\n");
            break;
        }
        send(peerTCPInfo.connectionFD, send_buff, buffersSize, 0);
    }

    pthread_join(recvLoopThreadID, &recvLoopRetval);

    closeTCPSocket(peerTCPInfo.connectionFD);
    closeTCPSocket(serverSocketFD);
    return 0;
}

void *recvLoop(void *args) {
    char recv_buff[buffersSize];
    TCPInfo *threadLocalPeerTCPInfo = (TCPInfo*)args;

    while (!shouldExit) {
        memset(recv_buff, '\0', buffersSize);
        recv(threadLocalPeerTCPInfo->connectionFD, recv_buff, buffersSize, 0);
        if (strcmp("exit", recv_buff) == 0) {
            printf("Saindo a pedido do client.\n");
            shouldExit = 1;
            fprintf(stdin, "exit\n");
            break;
        }
        printf("\nclient> %s\nserver> ", recv_buff);
        fflush(stdout);
    }
}
