/**
 * Biblioteca para facilitar o uso de TCP/IP no Linux.
 * Para mais informações consultar os seguintes manuais:
 * 
 * SOCKET(2)
 * IP(7)
 * BIND(2)
 * LISTEN(2)
 * ACCEPT(2)
 * CONNECT(2)
 * CLOSE(2)
 * ERRNO(3)
*/

#include <stdio.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#ifndef _TCP_H_
#define _TCP_H_

typedef struct TCPInfo {
    int connectionFD;
    struct sockaddr_in cliaddr;
    socklen_t addrlen;
} TCPInfo;

/**
 * Abre um socket TCP que pode ser usado tanto como servidor,
 * como cliente. Em caso de erro imprime a mensagem de erro
 * na saída padrão.
 * 
 * @return Em caso de sucesso retorna um descritor de arquivo (inteiro positivo). Caso contrário retorna -1.
 * @author Flávio Amaral e Silva
*/
int openTCPSocket(void);

/**
 * Executa bind num socket TCP gerado pela função openTCPSocket.
 * Em caso de erro imprime a mensagem de erro
 * na saída padrão.
 * 
 * @param ip IP (na notação x.x.x.x) para qual o bind será executado. Deve ser o ip de uma interface expecífica ou a interface indefinida (0.0.0.0).
 * @param port Porta na qual o bind será executado. Note que portas <= 1024 são privilegiadas.
 * @param socketFD Descritor de arquivo gerado pela função openTCPSocket.
 * @return Em caso de sucesso retorna 0, caso contrário retorna 1.
 * @see openTCPSocket
 * @author Flávio Amaral e Silva
*/
int bindTCPSocket(char *ip, unsigned short port, int socketFD);

/**
 * Executa listen num socket TCP gerado pela função openTCPSocket.
 * O socket deve ter passado previamente pela operação bind.
 * Em caso de erro imprime a mensagem de erro
 * na saída padrão.
 * 
 * @param socketFD Descritor de arquivo gerado pela função openTCPSocket.
 * @return Em caso de sucesso retorna 0, caso contrário retorna 1.
 * @see openTCPSocket
 * @see bindTCPSocket
 * @author Flávio Amaral e Silva
*/
int listenOnTCPSocket(int socketFD);

/**
 * Executa accept num num socket que já está no estado ~listen~
 * isto é, o socket foi criado com openTCPSocket, em seguida
 * foi realizado bind com bindTCPSocket e em seguida o socket
 * foi colocado no estado ~listen~ com listenOnTCPSocket.
 * 
 * @param socketFD Descritor de arquivo do socket que passou pelo processo descrito acima.
 * @return Em caso de sucesso retorna a estrutura ClientTCPInfo com a informação do cliente populada e o descritor de arquivo para a nova conexão. Em caso de falha retorna o descritor de arquivo -1 e a informação do cliente nula.
 * @see openTCPSocket
 * @see bindTCPSocket
 * @see listenOnTCPSocket
 * @author Flávio Amaral e Silva
*/
TCPInfo acceptNewTCPConnection(int socketFD);

/**
 * Conecta o cliente ao servidor, executando connect no socket
 * aberto com openTCPSocket.
 * 
 * @param ip IP na qual o socket irá se conectar
 * @param port Porta na qual o socket irá se conectar
 * @param socketFD Descritor de arquivo do socket cliente, aberto previanmente com openTCPSocket
 * no qual o socket deve ser conectado.
 * @return Retorna 0 em caso de sucesso. Retorna 1 caso contrário.
 * @see openTCPSocket
 * @author Flávio Amaral e Silva
*/
int connectTCPSocket(char *ip, unsigned short port, int socketFD);

/**
 * Fecha descritores de arquivos abertos durantes as operações com sockets.
 * Em caso de sucesso retorna 0, caso contrário retorna 1;
 * 
 * @param socketFD Descritor de arquivo gerado pela função openTCPSocket, ou acceptNewTCPConnection.
 * @return Em caso de sucesso retorna 0, caso contrário retorna 1.
 * @see openTCPSocket
 * @see acceptNewTCPConnection
 * @author Flávio Amaral e Silva
*/
int closeTCPSocket(int socketFD);

#endif /* _TCP_H_ */
